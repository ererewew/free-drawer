﻿// MDITemplate.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MDITemplate.h"
#include <CommCtrl.h>
#include <commdlg.h>
#include "WindowInfo.h"
#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

#define IMAGE_WIDTH     16
#define IMAGE_HEIGHT    16
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0

HWND hClient = nullptr;
HWND hToolBarWnd = nullptr;
HWND hWnd = nullptr;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
ATOM                RegisterChildClass(HINSTANCE hInstance);

BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    ChildProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);


/* When child window is created, initilize its info (extra byte) values to default*/
void initDefaultWndInfo(HWND hWnd);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MDITEMPLATE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);
	RegisterChildClass(hInstance);
    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MDITEMPLATE));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateMDISysAccel(hClient, &msg) && !TranslateAccelerator(hWnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = sizeof(int); // store number of child windows
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MDITEMPLATE));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDI_MDITEMPLATE);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

ATOM RegisterChildClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = ChildProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = sizeof(WindowInfo);
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MDITEMPLATE));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = L"MDI_CHILD";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}


void CreateToolbar(HWND hWnd)
{
	// loading Common Control DLL
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
		// Zero-based Bitmap image, ID of command, Button state, Button style, 
		// ...App data, Zero-based string (Button's label)
		{ STD_FILENEW,	IDM_FILENEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	IDM_FILEOPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	IDM_FILESAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	};
	// create a toolbar
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));
	







	/* ----------------------Add custom icon------------------------ */

	// define new buttons
	TBBUTTON tbButtonsCustom[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, 0,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, 0,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3, 0,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },

	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP1  };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);


	// identify the bitmap index of each button
	tbButtonsCustom[1].iBitmap += idx;
	tbButtonsCustom[2].iBitmap += idx;
	tbButtonsCustom[3].iBitmap += idx;
	tbButtonsCustom[4].iBitmap += idx;

	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtonsCustom) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtonsCustom);

}
void CreateClientWindow(HWND hWnd)
{
	CLIENTCREATESTRUCT ccs;
	ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 0);
	ccs.idFirstChild = IDM_WINDOW_1ST_CHILD;
	hClient = CreateWindow(L"MDICLIENT", nullptr,
		WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL,
		0, 0, 0, 0, hWnd, nullptr, hInst, &ccs);
	ShowWindow(hClient, SW_SHOW);

}

LRESULT CALLBACK MDICLoseProc(HWND hMDIWnd, LPARAM lParam)
{
	SendMessage(hClient, WM_MDIDESTROY, (WPARAM)hMDIWnd, 0);
	return true;
}


void createChildWindow(HWND hWnd)
{
	MDICREATESTRUCT mdiCreate;

	mdiCreate.szClass = L"MDI_CHILD";
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = 0;

	wchar_t buff[20];
	
	// update number of child windows in frame window class info
	int numChildWindow = GetClassLong(hWnd, 0);
	++numChildWindow;
	SetClassLong(hWnd, 0, numChildWindow);

	wsprintf(buff, L"Noname-%d.drw", numChildWindow);

	mdiCreate.szTitle = buff;
	SendMessage(hClient, WM_MDICREATE, 0, (LPARAM)(&mdiCreate));
}
//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
			case IDM_FILENEW:
				createChildWindow(hWnd);
				break;
			case IDM_FILEOPEN:
				MessageBox(hWnd, L"Bạn đã chọn Open", L"Thông báo", MB_OK);
				break;
			case IDM_FILESAVE:
				MessageBox(hWnd, L"Bạn đã chọn Save", L"Thông báo", MB_OK);
				break;
			case ID_DRAW_COLOR:
			{
				CHOOSECOLOR cc; // CTDL dùng cho dialog ChooseColor
				COLORREF acrCustClr[16]; // Các màu do user định nghĩa
				DWORD rgbCurrent = RGB(255, 0, 0); // màu được chọn default
												   // Khởi tạo struct
				ZeroMemory(&cc, sizeof(CHOOSECOLOR));
				cc.lStructSize = sizeof(CHOOSECOLOR);
				cc.hwndOwner = hWnd; // handle của window cha
				cc.lpCustColors = (LPDWORD)acrCustClr;
				cc.rgbResult = rgbCurrent; // trả về màu được chọn
				cc.Flags = CC_FULLOPEN | CC_RGBINIT;
				if (ChooseColor(&cc))
				{
					// xử lý màu được chọn, vd. tạo brush
					HBRUSH hbrush;
					hbrush = CreateSolidBrush(cc.rgbResult);
					rgbCurrent = cc.rgbResult;
				}

				break;
			}
			case ID_WINDOW_TIDE:
				SendMessage(hClient, WM_MDITILE, MDITILE_VERTICAL, 0);
				break;
			case ID_WINDOW_CASCADE:
				SendMessage(hClient, WM_MDICASCADE, MDITILE_ZORDER, 0);
				break;
			case ID_WINDOW_CLOSEALL:
				EnumChildWindows(hClient, (WNDENUMPROC)MDICLoseProc, 0);
				break;
            default:
                return DefFrameProc(hWnd, hClient, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		PostQuitMessage(0);
        break;
	case WM_SIZE:
	{
		RECT rect;
		GetWindowRect(hToolBarWnd, &rect);
		MoveWindow(hClient, 0, rect.bottom - rect.top, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), true);
		break;	
	}
	case WM_CREATE:
	{
		CreateClientWindow(hWnd);
		CreateToolbar(hWnd);
		break;
	}
	default:

		return 	DefFrameProc(hWnd, hClient, message, wParam, lParam);
    }
	return 0;
}


LRESULT CALLBACK ChildProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		//PostQuitMessage(0);
		break;
	case WM_CREATE:
	{
		initDefaultWndInfo(hWnd);
		break;
	}
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}


// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void initDefaultWndInfo(HWND hWnd)
{
	WindowInfo* wndInfo = new WindowInfo();
	wndInfo->color = RGB(0, 0, 0);
	wndInfo->hFont = CreateFont(0, 0, 0, 0, 0, false, false, false, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DECORATIVE, NULL);


	SetWindowLong(hWnd, 0, (LONG)wndInfo);
}
